package com.utp.administrativeStructure;

public class Faculty {

    private String facultyID;
    private String facultyName;
    private int asociatedPeople;
    private int asociatedStudents;
    private int asociatedWorkers;
    private int asociatedTeachers;
    private int asociatedInvestigators;
    private int asociatedGraduates;
    private int asociatedDegrees;

    public Faculty(String facultyID, String facultyName, int asociatedPeople, int asociatedStudents, int asociatedWorkers, int asociatedTeachers, int asociatedInvestigators, int asociatedGraduates, int asociatedDegrees) {
        this.facultyID = facultyID;
        this.facultyName = facultyName;
        this.asociatedPeople = asociatedPeople;
        this.asociatedStudents = asociatedStudents;
        this.asociatedWorkers = asociatedWorkers;
        this.asociatedTeachers = asociatedTeachers;
        this.asociatedInvestigators = asociatedInvestigators;
        this.asociatedGraduates = asociatedGraduates;
        this.asociatedDegrees = asociatedDegrees;
    }

    public String getFacultyID() {
        return this.facultyID;
    }

    public void setFacultyID(String facultyID) {
        this.facultyID = facultyID;
    }

    public String getFacultyName() {
        return this.facultyName;
    }

    public void setFacultyName(String facultyName) {
        this.facultyName = facultyName;
    }

    public int getAsociatedPeople() {
        return this.asociatedPeople;
    }

    public void setAsociatedPeople(int asociatedPeople) {
        this.asociatedPeople = asociatedPeople;
    }

    public int getAsociatedStudents() {
        return this.asociatedStudents;
    }

    public void setAsociatedStudents(int asociatedStudents) {
        this.asociatedStudents = asociatedStudents;
    }

    public int getAsociatedWorkers() {
        return this.asociatedWorkers;
    }

    public void setAsociatedWorkers(int asociatedWorkers) {
        this.asociatedWorkers = asociatedWorkers;
    }

    public int getAsociatedTeachers() {
        return this.asociatedTeachers;
    }

    public void setAsociatedTeachers(int asociatedTeachers) {
        this.asociatedTeachers = asociatedTeachers;
    }

    public int getAsociatedInvestigators() {
        return this.asociatedInvestigators;
    }

    public void setAsociatedInvestigators(int asociatedInvestigators) {
        this.asociatedInvestigators = asociatedInvestigators;
    }

    public int getAsociatedGraduates() {
        return this.asociatedGraduates;
    }

    public void setAsociatedGraduates(int asociatedGraduates) {
        this.asociatedGraduates = asociatedGraduates;
    }

    public int getAsociatedDegrees() {
        return this.asociatedDegrees;
    }

    public void setAsociatedDegrees(int asociatedDegrees) {
        this.asociatedDegrees = asociatedDegrees;
    }
}
