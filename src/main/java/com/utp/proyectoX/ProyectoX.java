package com.utp.proyectoX;

import com.utp.databaseManagement.PHP;
import com.utp.interfaces.mainWindow;
import javax.swing.JFrame;
import javax.swing.UIManager;

public class ProyectoX {

    public static void main(String[] args) {
        
   // new PHP().getTags(null, null).forEach(i->System.out.println(i.getMarc21Id()));
        

        try {

            JFrame.setDefaultLookAndFeelDecorated(true);

            //UIManager.setLookAndFeel("com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel");
            //UIManager.setLookAndFeel("javax.swing.plaf.metal.MetalLookAndFeel");
            UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
            //UIManager.setLookAndFeel("com.sun.java.swing.plaf.motif.MotifLookAndFeel");
        } catch (Exception e) {
            e.printStackTrace();
        }

    new mainWindow().setVisible(true);
 
    }

}
