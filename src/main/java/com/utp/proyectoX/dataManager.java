package com.utp.proyectoX;

import java.awt.Component;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class dataManager {

    public void checkStudentForm(JPanel panel) throws Exception {

        Component[] children = panel.getComponents();
        for (int i = 0; i < children.length; i++) {
            if (children[i] instanceof JTextField) {

                if (((JTextField) children[i]).getText().isEmpty()) {
                    if (((JTextField) children[i]).isEnabled()) {
                        ((JTextField) children[i]).grabFocus();
                        throw new Exception("Por favor llene todos los campos de este formulario");
                    }
                }
            }
        }

    }
}
