package com.utp.dataManagement;

public class DocumentType {

    private int index;
    private String name;
    private String description;
    private String type;
    private int usage;
    private int quantity;

    public DocumentType(int index, String Name, String description, String type, int usage) {
        this.index = index;
        this.name = Name;
        this.description = description;
        this.type = type;
        this.quantity = quantity;
    }

    public DocumentType(int index, String Name, String description, String type) {
        this.index = index;
        this.name = Name;
        this.description = description;
        this.type = type;
    }

    public DocumentType(int index, String Name, int usage) {
        this.index = index;
        this.name = Name;
        this.usage = usage;
    }

    public int getIndex() {
        return this.index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return this.type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getUsage() {
        return this.usage;
    }

    public void setUsage(int usage) {
        this.usage = usage;
    }

    public int getQuantity() {
        return this.quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getId() {
        String chars = this.getIndex() + "";
        int dim = chars.length();
        for (int i = 0; i < (4 - dim); i++) {
            chars = "0" + chars;
        }
        return ("DOC-" + chars);
    }
}
