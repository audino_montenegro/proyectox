package com.utp.interfaces;
//Interfaz principal del programa

import com.utp.dataManagement.Reports;
import com.utp.databaseManagement.MySQL;

public class mainWindow extends javax.swing.JFrame {

    public mainWindow() {
        initComponents();
        jDesktopPaneField.setBorder(new BackgroundImage(10, 10));
        new interfaceDuties().setScreenSize(this);
        /*  jInternalFrameTagRegister w = new jInternalFrameTagRegister();
         jDesktopPaneField.add(w);
         w.setVisible(true);*/
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jMenuItem1 = new javax.swing.JMenuItem();
        jMenu2 = new javax.swing.JMenu();
        jMenuItem2 = new javax.swing.JMenuItem();
        jDesktopPaneField = new javax.swing.JDesktopPane();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        jMenu6 = new javax.swing.JMenu();
        jMenuItemEstudiantes = new javax.swing.JMenuItem();
        jMenuItemDocumentos = new javax.swing.JMenuItem();
        jMenu3 = new javax.swing.JMenu();
        jMenu5 = new javax.swing.JMenu();
        jMenuItem5 = new javax.swing.JMenuItem();
        jMenuItem6 = new javax.swing.JMenuItem();
        jMenuItem7 = new javax.swing.JMenuItem();
        jMenu4 = new javax.swing.JMenu();
        jMenuItemEstudiantes1 = new javax.swing.JMenuItem();
        jMenuItem8 = new javax.swing.JMenuItem();
        jMenuItem9 = new javax.swing.JMenuItem();
        jMenuItemDocumentos1 = new javax.swing.JMenuItem();
        jMenu7 = new javax.swing.JMenu();
        jMenuItem3 = new javax.swing.JMenuItem();
        jMenuItem10 = new javax.swing.JMenuItem();
        jMenuItem4 = new javax.swing.JMenuItem();
        jMenu8 = new javax.swing.JMenu();
        jMenu9 = new javax.swing.JMenu();
        jMenu10 = new javax.swing.JMenu();
        jMenuItem12 = new javax.swing.JMenuItem();
        jMenuItem11 = new javax.swing.JMenuItem();
        jMenuItem13 = new javax.swing.JMenuItem();

        jMenuItem1.setText("jMenuItem1");

        jMenu2.setText("jMenu2");

        jMenuItem2.setText("jMenuItem2");

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Sistema de gestión de Biblioteca");
        setResizable(false);

        jMenu1.setText("Personal");

        jMenu6.setText("Registro de personal");

        jMenuItemEstudiantes.setText("Estudiante");
        jMenuItemEstudiantes.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItemEstudiantesActionPerformed(evt);
            }
        });
        jMenu6.add(jMenuItemEstudiantes);

        jMenuItemDocumentos.setText("Docente");
        jMenuItemDocumentos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItemDocumentosActionPerformed(evt);
            }
        });
        jMenu6.add(jMenuItemDocumentos);

        jMenu1.add(jMenu6);

        jMenuBar1.add(jMenu1);

        jMenu3.setText("Biblioteca");

        jMenu5.setText("Préstamo de Documentos");

        jMenuItem5.setText("Nuevo Préstamo");
        jMenuItem5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem5ActionPerformed(evt);
            }
        });
        jMenu5.add(jMenuItem5);

        jMenuItem6.setText("Devolución de Documento");
        jMenu5.add(jMenuItem6);

        jMenuItem7.setText("Préstamos activos");
        jMenuItem7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem7ActionPerformed(evt);
            }
        });
        jMenu5.add(jMenuItem7);

        jMenu3.add(jMenu5);

        jMenu4.setText("Administración de Etiquetas");

        jMenuItemEstudiantes1.setText("Catálogo");
        jMenuItemEstudiantes1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItemEstudiantes1ActionPerformed(evt);
            }
        });
        jMenu4.add(jMenuItemEstudiantes1);

        jMenuItem8.setText("Agregar Etiquetas");
        jMenu4.add(jMenuItem8);

        jMenuItem9.setText("Eliminar Etiquetas");
        jMenu4.add(jMenuItem9);

        jMenuItemDocumentos1.setText("Asignación de Etiquetas a Documentos");
        jMenuItemDocumentos1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItemDocumentos1ActionPerformed(evt);
            }
        });
        jMenu4.add(jMenuItemDocumentos1);

        jMenu3.add(jMenu4);

        jMenu7.setText("Administración de Colección");

        jMenuItem3.setText("Tipos de Documentos");
        jMenuItem3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem3ActionPerformed(evt);
            }
        });
        jMenu7.add(jMenuItem3);

        jMenuItem10.setText("Crear Título");
        jMenuItem10.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem10ActionPerformed(evt);
            }
        });
        jMenu7.add(jMenuItem10);

        jMenu3.add(jMenu7);

        jMenuItem4.setText("jMenuItem4");
        jMenuItem4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem4ActionPerformed(evt);
            }
        });
        jMenu3.add(jMenuItem4);

        jMenuBar1.add(jMenu3);

        jMenu8.setText("Departamentos");

        jMenu9.setText("Facultades");

        jMenu10.setText("Carreras");

        jMenuItem12.setText("Ver carreras");
        jMenu10.add(jMenuItem12);

        jMenuItem11.setText("Crear carrera");
        jMenuItem11.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem11ActionPerformed(evt);
            }
        });
        jMenu10.add(jMenuItem11);

        jMenuItem13.setText("Eliminar carrera");
        jMenu10.add(jMenuItem13);

        jMenu9.add(jMenu10);

        jMenu8.add(jMenu9);

        jMenuBar1.add(jMenu8);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jDesktopPaneField, javax.swing.GroupLayout.DEFAULT_SIZE, 234, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jDesktopPaneField, javax.swing.GroupLayout.DEFAULT_SIZE, 149, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jMenuItemEstudiantesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemEstudiantesActionPerformed

        jInternalFrameRegEst reg = new jInternalFrameRegEst();
        reg.setVisible(true);
        reg.pack();
        jDesktopPaneField.add(reg);
    }//GEN-LAST:event_jMenuItemEstudiantesActionPerformed

    private void jMenuItemDocumentosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemDocumentosActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jMenuItemDocumentosActionPerformed

    private void jMenuItemEstudiantes1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemEstudiantes1ActionPerformed
//Abrir ventana
        jInternalFrameTagAdministrator w = new jInternalFrameTagAdministrator();
        w.setVisible(true);
        jDesktopPaneField.add(w);


    }//GEN-LAST:event_jMenuItemEstudiantes1ActionPerformed

    private void jMenuItem3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem3ActionPerformed
        jInternalFrameDocumentType dc = new jInternalFrameDocumentType(jDesktopPaneField);
        dc.setVisible(true);
        jDesktopPaneField.add(dc);
    }//GEN-LAST:event_jMenuItem3ActionPerformed

    private void jMenuItem4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem4ActionPerformed
        new Reports().generateSingleStudentReport(new MySQL().getStudents());
    }//GEN-LAST:event_jMenuItem4ActionPerformed

    private void jMenuItem5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem5ActionPerformed
        jInternalFrameAlquiler dc = new jInternalFrameAlquiler();
        dc.setVisible(true);
        jDesktopPaneField.add(dc);

    }//GEN-LAST:event_jMenuItem5ActionPerformed

    private void jMenuItemDocumentos1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemDocumentos1ActionPerformed
        JInternalFrameEtiquetasMarc21 dc = new JInternalFrameEtiquetasMarc21();
        dc.setVisible(true);
        jDesktopPaneField.add(dc);
    }//GEN-LAST:event_jMenuItemDocumentos1ActionPerformed

    private void jMenuItem10ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem10ActionPerformed

        JInternalFrameNewBook dc = new JInternalFrameNewBook();
        dc.setVisible(true);
        jDesktopPaneField.add(dc);


    }//GEN-LAST:event_jMenuItem10ActionPerformed

    private void jMenuItem11ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem11ActionPerformed
        jInternalFrameCreateDegree dc = new jInternalFrameCreateDegree();
        dc.setVisible(true);
        jDesktopPaneField.add(dc);
    }//GEN-LAST:event_jMenuItem11ActionPerformed

    private void jMenuItem7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem7ActionPerformed

        jInternalFrameActiveTransaction dc = new jInternalFrameActiveTransaction();
        dc.setVisible(true);
        jDesktopPaneField.add(dc);

    }//GEN-LAST:event_jMenuItem7ActionPerformed

    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(mainWindow.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(mainWindow.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(mainWindow.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(mainWindow.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new mainWindow().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JDesktopPane jDesktopPaneField;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu10;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenu jMenu3;
    private javax.swing.JMenu jMenu4;
    private javax.swing.JMenu jMenu5;
    private javax.swing.JMenu jMenu6;
    private javax.swing.JMenu jMenu7;
    private javax.swing.JMenu jMenu8;
    private javax.swing.JMenu jMenu9;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JMenuItem jMenuItem10;
    private javax.swing.JMenuItem jMenuItem11;
    private javax.swing.JMenuItem jMenuItem12;
    private javax.swing.JMenuItem jMenuItem13;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JMenuItem jMenuItem3;
    private javax.swing.JMenuItem jMenuItem4;
    private javax.swing.JMenuItem jMenuItem5;
    private javax.swing.JMenuItem jMenuItem6;
    private javax.swing.JMenuItem jMenuItem7;
    private javax.swing.JMenuItem jMenuItem8;
    private javax.swing.JMenuItem jMenuItem9;
    private javax.swing.JMenuItem jMenuItemDocumentos;
    private javax.swing.JMenuItem jMenuItemDocumentos1;
    private javax.swing.JMenuItem jMenuItemEstudiantes;
    private javax.swing.JMenuItem jMenuItemEstudiantes1;
    // End of variables declaration//GEN-END:variables
}
