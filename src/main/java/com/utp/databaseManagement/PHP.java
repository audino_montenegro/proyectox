package com.utp.databaseManagement;

import com.utp.administrativeStructure.Degree;
import com.utp.administrativeStructure.Faculty;
import com.utp.dataManagement.DocumentType;
import com.utp.dataManagement.Marc21;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

public class PHP {

    public List<Faculty> getFaculties(String parameter, String value) {
        String json = getJSON("faculty", parameter, value);
        Object jsonObject = JSONValue.parse(json.toString());
        JSONArray array = (JSONArray) jsonObject;
        List<Faculty> lista = new ArrayList<Faculty>();
        for (int i = 0; i < array.size(); i++) {
            JSONObject row = (JSONObject) array.get(i);
            lista.add(new Faculty(
                    row.get("faculty_id").toString(),
                    row.get("faculty_name").toString(),
                    Integer.parseInt(row.get("associated_people").toString()),
                    Integer.parseInt(row.get("associated_students").toString()),
                    Integer.parseInt(row.get("associated_workers").toString()),
                    Integer.parseInt(row.get("associated_teachers").toString()),
                    Integer.parseInt(row.get("associated_investigators").toString()),
                    Integer.parseInt(row.get("associated_graduates").toString()),
                    Integer.parseInt(row.get("associated_degrees").toString())));

        }
        return lista;
    }

    public List<Degree> getDegrees(String parameter, String value) {
        String json = getJSON("degree", parameter, value);
        Object jsonObject = JSONValue.parse(json.toString());
        JSONArray array = (JSONArray) jsonObject;
        List<Degree> lista = new ArrayList<Degree>();
        for (int i = 0; i < array.size(); i++) {
            JSONObject row = (JSONObject) array.get(i);
            lista.add(new Degree(
                    row.get("degree_id").toString(),
                    row.get("faculty_id").toString(),
                    row.get("degree_name").toString(),
                    row.get("description").toString(),
                    row.get("in_profile").toString(),
                    row.get("out_profile").toString(),
                    Double.parseDouble(row.get("cost").toString()),
                    row.get("workplace").toString(),
                    Integer.parseInt(row.get("semesters").toString()),
                    row.get("study_mode").toString(),
                    row.get("title").toString(),
                    Integer.parseInt(row.get("associated_students").toString()),
                    Integer.parseInt(row.get("associated_graduates").toString()),
                    row.get("status").toString()));

        }
        return lista;
    }

    private String getJSON(String tableName, String parameter, String value) {

        StringBuffer response = null;
        try {
            String url = "";
            if (parameter == null) {
                url = "http://sc3i.com/magi/get_info.php?table=" + tableName + "&parameter=null&value=null";
            } else {
                url = "http://sc3i.com/magi/get_info.php?table=" + tableName + "&parameter=" + parameter + "&value=" + value;
            }
            URL obj = new URL(url);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();
            con.setRequestMethod("POST");
            con.setDoOutput(true);
            int responseCode = con.getResponseCode();
            BufferedReader in = new BufferedReader(
                    new InputStreamReader(con.getInputStream()));
            String inputLine;
            response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }

            in.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return response.toString();
    }

    public boolean saveDegrees(Degree deg) {

        try {
            // open a connection to the site
            URL url = new URL("http://www.sc3i.com/magi/save_degree.php");
            URLConnection con = url.openConnection();
            // activate the output
            con.setDoOutput(true);
            PrintStream ps = new PrintStream(con.getOutputStream());
            // send your parameters to your site

            ps.print("degrees_id=" + deg.getDegreeID());
            ps.print("&faculty_id=" + deg.getFacultyID());
            ps.print("&degree_name=" + deg.getDegreeName());
            ps.print("&description=" + deg.getDescription());
            ps.print("&&in_profile=" + deg.getInProfile());
            ps.print("&out_profile=" + deg.getOutProfile());
            ps.print("&cost=" + deg.getCost());
            ps.print("&workplace=" + deg.getWorkplace());
            ps.print("&semesters=" + deg.getSemesters());
            ps.print("&study_mode=" + deg.getStudyMode());
            ps.print("&title=" + deg.getTitle());
            ps.print("&asociated_students=" + deg.getAsociatedStudents());
            ps.print("&asociated_graduates=" + deg.getAsociatedGraduates());
            ps.print("&status=" + deg.getStatus());

            // we have to get the input stream in order to actually send the request
            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            String line = null;
            while ((line = in.readLine()) != null) {
                System.out.println(line);
            }

            // close the print stream
            ps.close();
            return true;
        } catch (MalformedURLException e) {
            e.printStackTrace();
            return false;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }

    }

    public List<Marc21> getTags(String parameter, String value) {
        List<Marc21> tags = new ArrayList<Marc21>();
        String json = getJSON("marc21", parameter, value);
        Object jsonObject = JSONValue.parse(json.toString());
        JSONArray array = (JSONArray) jsonObject;

        for (int i = 0; i < array.size(); i++) {
            JSONObject row = (JSONObject) array.get(i);
            tags.add(new Marc21(
                    Integer.parseInt(row.get("marc21_id").toString()),
                    row.get("tag").toString(),
                    row.get("name").toString(),
                    row.get("description").toString()
            ));

        }
        
        return tags;
    }

    public List<DocumentType> getDocumentTypes() {

        List<DocumentType> lista = new ArrayList<DocumentType>();

        String json = getJSON("document_type", null, null);
        Object jsonObject = JSONValue.parse(json.toString());
        JSONArray array = (JSONArray) jsonObject;

        for (int i = 0; i < array.size(); i++) {
            JSONObject row = (JSONObject) array.get(i);
            lista.add(new DocumentType(
                    Integer.parseInt(row.get("doctype_id").toString()),
                    row.get("name").toString(),
                    row.get("description").toString(),
                    row.get("type").toString(),
                    Integer.parseInt(row.get("stock").toString())));

        }

        return lista;
    }

}
