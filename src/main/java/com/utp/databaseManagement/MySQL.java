package com.utp.databaseManagement;

import com.utp.dataManagement.DocumentType;
import com.utp.dataManagement.Marc21;
import com.utp.dataManagement.Student;
import com.utp.peopleManagement.Estudiante;
import com.mysql.jdbc.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

public class MySQL {

    private static Connection Conexion;
    private String user = "root";
    private String pass = "";
    private String db_name = "rfid";

    public void startMySqlConnection(String user, String pass, String db_name) {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            Conexion = (Connection) DriverManager.getConnection("jdbc:mysql://localhost:3306/" + db_name, user, pass);
            //  JOptionPane.showMessageDialog(null, "Se ha iniciado la conexión con el servidor de forma exitosa");
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(MySQL.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(MySQL.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void stopMySqlConnection() {
        try {
            Conexion.close();
        } catch (SQLException ex) {
            Logger.getLogger(MySQL.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Solo devuelve las etiquetas sin especificar el uso
     *
     * @return Una lista con todas las etiquetas del sistema Marc21
     */
    public List<Marc21> getTags() {
        List<Marc21> tags = new ArrayList<Marc21>();
        this.startMySqlConnection(user, pass, db_name);
        try {

            String Query = "SELECT * FROM Marc21";
            Statement st = Conexion.createStatement();
            java.sql.ResultSet resultSet;
            resultSet = st.executeQuery(Query);
            while (resultSet.next()) {
                tags.add(new Marc21(Integer.parseInt(resultSet.getString("Marc21Id")), resultSet.getString("Etiqueta"), resultSet.getString("Nombre"), resultSet.getString("Descripcion")));
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error en la adquisición de datos");
        }
        this.stopMySqlConnection();
        return tags;

    }

    /**
     * Este método se utilza para las tablas de selección de etiquetas Marc21
     *
     * @param docTypeId: ID del tipo de documento
     * @return Una lista de etiquetas Marc21 y su uso según el tipo de documento
     */
    public List<Marc21> getTags(int docTypeId) {
        List<Marc21> tags = new ArrayList<Marc21>();
        this.startMySqlConnection(user, pass, db_name);
        try {

            String Query = "SELECT * FROM Marc21";
            Statement st = Conexion.createStatement();
            java.sql.ResultSet resultSet;
            resultSet = st.executeQuery(Query);
            List<Integer> list = getMarc21Link(docTypeId);
            while (resultSet.next()) {
                tags.add(new Marc21(list.contains(resultSet.getInt("Marc21Id")), Integer.parseInt(resultSet.getString("Marc21Id")), resultSet.getString("Etiqueta"), resultSet.getString("Nombre"), resultSet.getString("Descripcion")));
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error en la adquisición de datos");
        }
        this.stopMySqlConnection();
        return tags;
        
      
    }

    private List<Integer> getMarc21Link(int tpdID) {
        List<Integer> list = new ArrayList<Integer>();

        this.startMySqlConnection(user, pass, db_name);
        try {

            String Query = "SELECT * FROM tpd_marc21 WHERE TipoId =\"" + tpdID + "\"";
            Statement st = Conexion.createStatement();
            java.sql.ResultSet resultSet;
            resultSet = st.executeQuery(Query);
            while (resultSet.next()) {
                list.add(resultSet.getInt("tpd_marc21Id"));
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error en la adquisición de datos");
        }
        this.stopMySqlConnection();

        return list;
    }

    public void saveDocumentTypeData(DocumentType doc) throws SQLException {
        this.startMySqlConnection(user, pass, db_name);

        String Query = "INSERT INTO tipo_de_documento VALUES("
                + "\"" + doc.getIndex() + "\", "
                + "\"" + doc.getName() + "\", "
                + "\"" + doc.getDescription() + "\", "
                + "\"" + doc.getType() + "\", "
                + "\"" + "0" + "\")";

        Statement st = Conexion.createStatement();
        st.executeUpdate(Query);
        JOptionPane.showMessageDialog(null, "Se ha guardado la información del documento exitosamente.");
        this.stopMySqlConnection();

    }

    public int getDocumentTypeCount() {
        this.startMySqlConnection(user, pass, db_name);
        int Output = 0;
        try {

            String Query = "SELECT * FROM tipo_de_documento";
            Statement st = Conexion.createStatement();
            java.sql.ResultSet resultSet;
            resultSet = st.executeQuery(Query);
            while (resultSet.next()) {
                Output++;
            }
            this.stopMySqlConnection();
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error en la adquisición de datos");
        }

        return Output;
    }

    public List<DocumentType> getDocumentTypes() {
        this.startMySqlConnection(user, pass, db_name);
        List<DocumentType> lista = new ArrayList<DocumentType>();
        try {

            String Query = "SELECT * FROM tipo_de_documento";
            Statement st = Conexion.createStatement();
            java.sql.ResultSet resultSet;
            resultSet = st.executeQuery(Query);
            while (resultSet.next()) {
                lista.add(new DocumentType(
                        resultSet.getInt("tpdId"),
                        resultSet.getString("Nombre"),
                        resultSet.getString("Descripcion"),
                        resultSet.getString("Tipo"),
                        resultSet.getInt("Existencias")));
            }
            this.stopMySqlConnection();
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error en la adquisición de datos");
        }

        return lista;
    }

    public void deleteDocumentType(String docID) {
        this.startMySqlConnection(user, pass, db_name);

        try {

            String Query = "DELETE FROM rfid_tipo_de_documento WHERE tpd_Id=";
            Statement st = Conexion.createStatement();
            java.sql.ResultSet resultSet;
            resultSet = st.executeQuery(Query);

            this.stopMySqlConnection();
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error en la adquisición de datos");
        }

    }

    public List<Estudiante> getStudents() {
        List<Estudiante> lista = new ArrayList<Estudiante>();

        lista.add(new Estudiante("Antony",
                "García", "30/7/1992", 22, "O+", "Chitré, provincia de Herrera",
                "M", "7-708-846", "antony.garcia.gonzalez@gmail.com", "antony.garcia@utp.ac.pa",
                "9946401", "67347398", "Panamá", "Panamá", "Bethania", "Bethania", "La Gloria, Bethania, Calle 18C Norte",
                "CD49C810", "Facultad de Ingeniería Eléctrica", "Licenciatura en Ingeniería Electromecánica", "Campus Víctor Levi Sasso"));
        return lista;
    }

    public List<Student> getStudents(String parameter, String value) {
        this.startMySqlConnection(user, pass, db_name);
        List<Student> lista = new ArrayList<Student>();
        try {

            String Query = "SELECT * FROM estudiante WHERE " + parameter + " = \"" + value + "\"";
            Statement st = Conexion.createStatement();
            java.sql.ResultSet resultSet;
            resultSet = st.executeQuery(Query);
            while (resultSet.next()) {
                lista.add(new Student(
                        resultSet.getString("Nombre"),
                        resultSet.getString("Apellido"),
                        resultSet.getString("FechaDeNacimiento"),
                        resultSet.getString("TipoDeSangre"),
                        resultSet.getString("EstudianteId"),
                        resultSet.getString("Email"),
                        resultSet.getString("Telefono"),
                        resultSet.getString("Celular"),
                        resultSet.getString("PaisDeOrigen"),
                        resultSet.getString("Provincia"),
                        resultSet.getString("Distrito"),
                        resultSet.getString("Corregimiento"),
                        resultSet.getString("Direccion"),
                        resultSet.getString("Campus"),
                        resultSet.getString("Facultad"),
                        resultSet.getString("Carrera"),
                        resultSet.getString("Rfid")));
            }
            this.stopMySqlConnection();
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "El estudiante consultado no existe en la base de datos");
        }

        return lista;
    }
}
