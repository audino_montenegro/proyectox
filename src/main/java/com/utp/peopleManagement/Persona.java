
/*
 La clase abstracta persona contendrá la información que le corresponde a 
 persona, independientemente de si es estudiante, docente y administrativo, etc.

 Las clases estudiante, docente, investigador, administrativo, egresado y particular
 heredrán los atributos de la clase Persona.
 */
package com.utp.peopleManagement;

public abstract class Persona {

    private String name;
    private String lastName;
    private String birthDate;
    private int age;
    private String bloodType;
    private String birthPlace;
    private String gender;
    private String id;
    private String email;
    private String emailUtp;
    private String phone;
    private String cellPhone;
    private String country;
    private String province;
    private String district;
    private String department;
    private String direction;
    private String rfid;

    public Persona(String name, String lastName, String birthDate, int age, String bloodType, String birthPlace, String gender, String id, String email, String emailUtp, String phone, String cellPhone, String country, String province, String district, String department, String direction, String rfid) {
        this.name = name;
        this.lastName = lastName;
        this.birthDate = birthDate;
        this.age = age;
        this.bloodType = bloodType;
        this.birthPlace = birthPlace;
        this.gender = gender;
        this.id = id;
        this.email = email;
        this.emailUtp = emailUtp;
        this.phone = phone;
        this.cellPhone = cellPhone;
        this.country = country;
        this.province = province;
        this.district = district;
        this.department = department;
        this.direction = direction;
        this.rfid = rfid;
    }

    public String getname() {
        return this.name;
    }

    public void setname(String name) {
        this.name = name;
    }

    public String getlastName() {
        return this.lastName;
    }

    public void setlastName(String lastName) {
        this.lastName = lastName;
    }

    public String getbirthdate() {
        return this.birthDate;
    }

    public void setbirthdate(String birthDate) {
        this.birthDate = birthDate;
    }

    public int getage() {
        return this.age;
    }

    public void setage(int age) {
        this.age = age;
    }

    public String getbloodtype() {
        return this.bloodType;
    }

    public void setbloodtype(String bloodType) {
        this.bloodType = bloodType;
    }

    public String getbirthplace() {
        return this.birthPlace;
    }

    public void setbirthplace(String birthPlace) {
        this.birthPlace = birthPlace;
    }

    public String getgender() {
        return this.gender;
    }

    public void setgender(String gender) {
        this.gender = gender;
    }

    public String getid() {
        return this.id;
    }

    public void setid(String id) {
        this.id = id;
    }

    public String getemail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmailUtp() {
        return this.emailUtp;
    }

    public void setEmailUtp(String emailUtp) {
        this.emailUtp = emailUtp;
    }

    public String getPhone() {
        return this.phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getCellPhone() {
        return this.cellPhone;
    }

    public void setCellPhone(String cellPhone) {
        this.cellPhone = cellPhone;
    }

    public String getCountry() {
        return this.country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getProvince() {
        return this.province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getDistrict() {
        return this.district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getDepartment() {
        return this.department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getDirection() {
        return this.direction;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    public String getRfid() {
        return this.rfid;
    }

    public void setRfid(String rfid) {
        this.rfid = rfid;
    }
}
