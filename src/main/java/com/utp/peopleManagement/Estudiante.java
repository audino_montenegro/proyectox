package com.utp.peopleManagement;

/*
 Esta clase hereda a la clase abstracta Persona.
 La clase estudiante heredará todos los atributos y los métodos de la clase persona
 Como se va a manejar diferentes tipos de personas (estudiantes, docentes, administrativos, etc)
 conviene crear una clase que contenga los atributos de cada ser humano y luego
 que las clases específicas hereden dichos atributos y posean aquellos muy específicos
 de la categoría a la que pertenecen.

 Por ejemplo, en la clase estudiante se hereda todo lo de la clase persona
 pero se agregan los atributos facultad, carrera y el campus en el que estudian.

 Los docentes, administrativos y demás también heredarán a la clase persona, aparte
 de los atributos específicos que les correspondan.
 */
public class Estudiante extends Persona {

    private String faculty;
    private String degree;
    private String campus;

    public Estudiante(String name, String lastName, String birthDate, int age, String bloodType, String birthPlace, String gender, String id, String email, String emailUtp, String phone, String cellPhone, String country, String province, String district, String department, String direction, String rfid, String faculty, String degree, String campus) {
        super(name, lastName, birthDate, age, bloodType, birthPlace, gender, id, email, emailUtp, phone, cellPhone, country, province, district, department, direction, rfid);

        this.faculty = faculty;
        this.degree = degree;
        this.campus = campus;
    }

    public String getfaculty() {
        return this.faculty;
    }

    public void setfaculty(String faculty) {
        this.faculty = faculty;
    }

    public String getdegree() {
        return this.degree;
    }

    public void setdegree(String degree) {
        this.degree = degree;
    }

    public String getcampus() {
        return this.campus;
    }

    public void setcampus(String campus) {
        this.campus = campus;
    }

}
